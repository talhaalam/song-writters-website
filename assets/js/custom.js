$( document ).ready(function() {
  
  // Sticky Header
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
        if (scroll >= 100){  
            $('.header').addClass("sticky");
        }
        else{
            $('.header').removeClass("sticky");
        }
    }); 
    

    $('.navbar-list').on('click', 'li', function() {
      $('li.active').removeClass('active');
      $(this).addClass('active');
    });

    // Mobile Menu
    $(".toggle").click(function() {
      $(this).toggleClass("on");
      $(".navbar-list").slideToggle();
    });
    
    // niceSelect
    $('select').niceSelect();

    // Data Table
    $('#songslist-table').DataTable({
      "searching": true,
      "paging": true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search Songs",
          paginate: {
              first:    '«',
              previous: '‹',
              next:     '›',
              last:     '»'
          },
          aria: {
              paginate: {
                  first:    'First',
                  previous: 'Previous',
                  next:     'Next',
                  last:     'Last'
              }
          }
        }
    });

    // Animated Counter
    var counted = 0;
    $(window).scroll(function() {
    var oTop = $('#counter').offset().top - window.innerHeight;
    if (counted == 0 && $(window).scrollTop() > oTop) {
        $('.count').each(function() {
        var $this = $(this),
            countTo = $this.attr('data-count');
        $({
            countNum: $this.text()
        }).animate({
            countNum: countTo
            },

            {

            duration: 1000,
            easing: 'swing',
            step: function() {
                $this.text(Math.floor(this.countNum));
            },
            complete: function() {
                $this.text(this.countNum);
                //alert('finished');
            }

            });
        });
        counted = 1;
    }
    });
    // photo-slider
    var swiper = new Swiper(".photo-slider", {
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      breakpoints: {
        320: {
          slidesPerView: 2,
          spaceBetween: 10
        },
        767: {
          slidesPerView: 4,
          spaceBetween: 15
        },
        1200: {
          slidesPerView: 5,
          spaceBetween: 47
        }
      }
  });
  // video-slider
  var swiper = new Swiper(".video-slider", {
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    breakpoints: {
      320: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      767: {
        slidesPerView: 4,
        spaceBetween: 15
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 32
      }
    }
  });

   // get video Time
   var myVideoPlayer = document.getElementById('myVideo'), meta = document.getElementById('meta');
   myVideoPlayer.addEventListener('loadedmetadata', function () {
     var duration = myVideoPlayer.duration;
     meta.innerHTML = duration.toFixed(2);
   });
   
      
});



